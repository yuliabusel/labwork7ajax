CREATE TABLE keywords (
  id      INT(11)     NOT NULL AUTO_INCREMENT,
  keyword VARCHAR(45) NOT NULL,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE classes (
  id        INT(11)     NOT NULL AUTO_INCREMENT,
  classname VARCHAR(45) NOT NULL,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE common (
  keywords_id INT(11) NOT NULL,
  classes_id  INT(11) NOT NULL,
  PRIMARY KEY (keywords_id, classes_id),
  KEY fk_keywords (keywords_id),
  KEY fk_classes (classes_id),
  CONSTRAINT fk_classes FOREIGN KEY (classes_id) REFERENCES classes (id),
  CONSTRAINT fk_keywords FOREIGN KEY (keywords_id) REFERENCES keywords (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

INSERT INTO common (keywords_id, classes_id) VALUES ('1', '26');
INSERT INTO common (keywords_id, classes_id) VALUES ('2', '28');
INSERT INTO common (keywords_id, classes_id) VALUES ('3', '27');
INSERT INTO common (keywords_id, classes_id) VALUES ('3', '28');

INSERT INTO classes (id, classname) VALUES ('26', 'Bell');
INSERT INTO classes (id, classname) VALUES ('27', 'Bulb');
INSERT INTO classes (id, classname) VALUES ('28', 'Computer');
INSERT INTO classes (id, classname) VALUES ('29', 'Microwave');

INSERT INTO keywords (id, keyword) VALUES ('1', 'ring');
INSERT INTO keywords (id, keyword) VALUES ('2', 'on');
INSERT INTO keywords (id, keyword) VALUES ('3', 'off');
INSERT INTO keywords (id, keyword) VALUES ('4', 'out');