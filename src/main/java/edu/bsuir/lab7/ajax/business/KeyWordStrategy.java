package edu.bsuir.lab7.ajax.business;

import edu.bsuir.lab7.ajax.entities.KeyWords;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import edu.bsuir.lab7.ajax.util.HibernateUtil;

import java.util.List;


public class KeyWordStrategy {
    private Session session;
    private Transaction transaction;
    private final String hql = "select distinct k.keyword from KeyWords k join k.classes t where t.classname=:classname";

    public List<KeyWords> find(String string) {
        session = HibernateUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery(hql);
        query.setParameter("classname", string);
        List<KeyWords> words = query.list();
        transaction.commit();
        session.close();
        return words;
    }
}
