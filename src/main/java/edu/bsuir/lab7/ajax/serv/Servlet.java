package edu.bsuir.lab7.ajax.serv;

import edu.bsuir.lab7.ajax.business.KeyWordStrategy;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/Servlet")
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("html/plain");
        String str = request.getParameter("name");
        KeyWordStrategy keyWordStrategy = new KeyWordStrategy();
        PrintWriter printWriter = response.getWriter();
        printWriter.print(keyWordStrategy.find(str));
    }
}
