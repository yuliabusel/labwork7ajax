package edu.bsuir.lab7.ajax.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static SessionFactory sessionFactory = buildSessionFactory();

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static SessionFactory buildSessionFactory() {
        return new Configuration().configure().buildSessionFactory();

    }
}
