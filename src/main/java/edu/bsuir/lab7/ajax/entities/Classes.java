package edu.bsuir.lab7.ajax.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "classes")
public class Classes {
    @Id
    @Column(name = "id")
    private int id;
    @Column(name = "classname")
    private String classname;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "common", joinColumns = {@JoinColumn(name = "classes_id")},
            inverseJoinColumns = {@JoinColumn(name = "keywords_id")})
    private Set<KeyWords> keyWordses = new HashSet<>();

    public Classes() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public Set<KeyWords> getKeyWordses() {
        return keyWordses;
    }

    public void setKeyWordses(Set<KeyWords> keyWordses) {
        this.keyWordses = keyWordses;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Classes)) return false;

        Classes classes = (Classes) o;

        if (id != classes.id) return false;
        if (classname != null ? !classname.equals(classes.classname) : classes.classname != null) return false;
        return !(keyWordses != null ? !keyWordses.equals(classes.keyWordses) : classes.keyWordses != null);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (classname != null ? classname.hashCode() : 0);
        result = 31 * result + (keyWordses != null ? keyWordses.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(" classname ").append(classname);
        sb.append(" keyWordses ").append(keyWordses);
        return sb.toString();
    }
}
