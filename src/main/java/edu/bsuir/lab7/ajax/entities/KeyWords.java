package edu.bsuir.lab7.ajax.entities;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "keywords")
public class KeyWords {
    @Id
    @Column(name = "id")
    private int id;
    @Column(name = "keyword")
    private String keyword;
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "keyWordses")
    private Set<Classes> classes = new HashSet<>();

    public KeyWords() {
    }

    public Set<Classes> getClasses() {
        return classes;
    }

    public void setClasses(Set<Classes> classes) {
        this.classes = classes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof KeyWords)) return false;

        KeyWords keyWords = (KeyWords) o;

        if (id != keyWords.id) return false;
        if (keyword != null ? !keyword.equals(keyWords.keyword) : keyWords.keyword != null) return false;
        return !(classes != null ? !classes.equals(keyWords.classes) : keyWords.classes != null);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (keyword != null ? keyword.hashCode() : 0);
        result = 31 * result + (classes != null ? classes.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(" keyword ").append(keyword);
        sb.append(" classes ").append(classes);
        return sb.toString();
    }
}
